#ifndef CELULAS_HPP
#define CELULAS_HPP

using namespace std

class Celula{
	private:
		int posx;
		int posy;
	public:
		Celulas();
		celulas(int posx, int posy);
		~Celulas();
		int getPosx();
		void setPosx(int getPosx);
		int getPosy();
		void setPosy(int getPosy);
		virtual void mudaMatriz(int posx, int posy, char** matriz);
};



#endif
