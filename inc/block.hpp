#ifndef BLOCK_HPP
#define BLOCK_HPP
#include "celulas.hpp"

using namespace std;

class Block:: public Celulas{
	public: 
		Block();
		Block(int posx, int posy);
		~Block();
		void mudaMatriz(int posx, posy, char **matriz);
};
