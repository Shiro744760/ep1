#ifndef GLIDER_HPP
#define GLIDER_HPP
#include "celulas.hpp"

using namespace std;

class Glider:: public Celulas{
	public:
		Glider();
		Glider(int posx, int posy);
		~Glider();
		void mudaMatriz(int posx, posy, char **matriz);
};
